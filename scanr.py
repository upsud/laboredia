#!/usr/bin/env python3

import json, requests, re, os
from bs4 import BeautifulSoup

fp = open("fr-esr-structures-recherche-publiques-actives.json")
scanr = json.load(fp)

#print(len(scanr))
#print(scanr[0].keys())

class ScanrLab:
    def __init__(self, nom, site=None):
        self.nom = nom
        self.site = site
        self.sites_alt = []
        self.sigle = None
        self.valid = False
        self.index = None
        self.meme_nom_que = []
        self.meme_site_que = []
        self.start_urls = None

    def valide(self, session):
        """Valide le site et essaie de trouver l'annuaire"""
        if not self.site:
            self.valid = False
            return
        try:
            p = session.get(self.site, timeout=10)
        except requests.exceptions.RequestException as e:
            print("timeout?", e)
            self.valid = False
            return
        if p.status_code==200:
            self.valid = True
            #if self.sigle:
            #    nom_fichier = self.sigle
            #else:
            nom_fichier = "%2F".join(self.site.split('/')[2:])
            print("code = 200 pour", self.site, "téléch dans", nom_fichier)
            fd = None
            try:
                fd = open("./sites/" + nom_fichier) # fichier existant ?
            except:
                pass
            if fd:
                if len(fd.read()):
                    self.valid = True # On a déjà le contenu de cette page d'accueil
                else:
                    self.valid = False # Fichier vide (erreur d'accès)
                return # Dans les 2 cas, on sort
            fd = open("./sites/" + nom_fichier, 'w')
            fd.write(p.text)
            fd.close()
            return
            #mot_annuaire = re.finditer("annuaire", p.text, re.I)
            #for m in mot_annuaire:
            #    if 'href' in p.text[m.span()[0] - 70: m.span()[1] + 70]:
            #        self.cherche_annuaire(p.text)
            #        break
        if p.status_code == 404:
            print("code = 404 pour", self.site)
            self.valid = False
        else:
            print("code =", p.status_code, "pour", self.site)

    def cherche_annuaire(self, html):
        try:
            soup = BeautifulSoup(html, 'html.parser')
        except:
            print("HTML imparsable")
            return
        liens_annuaire = []
        for x in soup.find_all('a'):
            if 'nnuaire' in str(x):
                href = x.get('href')
                href = href.strip()
                if href[-1] == '/':
                    href = href[:-1]
                if not href.startswith('http'):
                    href = 'http://' + (self.site + '/' + href).replace('//', '/').replace('/./', '/').replace('//', '/')
                if href and not href in liens_annuaire:
                    liens_annuaire.append(href)
        self.start_urls = liens_annuaire


class ScanrLabs(list):
    def __init__(self):
        self.labs = []

    def taille(self):
        return len(self.labs)

    def ajoute(self, l):
        self.labs.append(l)

    def repere_doublons(self):
        pas = 0
        for l1 in self.labs:
            #print(pas)
            pas += 1
            for l2 in self.labs:
                if l1.nom == l2.nom and l1.site == l2.site:
                    #print ("doublon complet, ou c'est le même")
                    continue
                if l1.nom == l2.nom:
                    if l2 not in l1.meme_nom_que:
                        l1.meme_nom_que.append(l2)
                    if l1 not in l2.meme_nom_que:
                        l2.meme_nom_que.append(l1)
                    #print("meme nom :", l1.nom, "pour", l1.site, "et", l2.site)
                if l1.site and l1.site == l2.site:
                    l1.meme_site_que.append(l2)
                    l2.meme_site_que.append(l1)
                    #print("meme site :", l1.site, "pour", l1.nom, "et", l2.nom)

    def valide(self):
        s = requests.Session()
        for l in self.labs:
            l.valide(s)

    def cherche_annuaires(self, rep):
        fichiers = os.listdir(rep)
        for lab in self.labs[0:200]:
            if not lab.site:
                continue
            print(lab.site)
            try:
                html = rep + '/' + lab.site.replace('/', '%2F')
            except:
                print("Fichier illisible", f)
                print()
                continue
            lab.cherche_annuaire(html)
            print(lab.start_urls)


labs = ScanrLabs()
for s in scanr:
    if not 'libelle' in s['fields'].keys():
        print(s)
        break
    alter = None
    if 'site_web_alternatif' in s['fields'].keys():
        alter = s['fields']['site_web_alternatif'].split(';')
    if not 'site_web' in s['fields'].keys() or '404' in s['fields']['site_web']:
        l = ScanrLab(s['fields']['libelle'])
        if alter:
            l.sites_alt = [ x.strip() for x in alter ]
    else:
        l = ScanrLab(s['fields']['libelle'], s['fields']['site_web'])
        if alter:
            l.sites_alt = [ x.strip() for x in alter ]
    if 'sigle' in s['fields'].keys():
        l.sigle = s['fields']['sigle']
    labs.ajoute(l)

#labs.repere_doublons()
#labs.valide()
labs.cherche_annuaires("./sites/")
