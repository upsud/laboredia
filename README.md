LABOREDIA Project wishes to offer controlled links
from Wikipedia Pages to Research Laboratories Web Pages
-------------------------------------------------------

On one side: Wikipedia users, all languages, with a Laboredia Javascript extension

On the other: Research Laboratories Web Sites, with teams, projects, researchers, topics and concepts

In the middle, Laboredia offers matches between a Wikipedia page and Laboratories, awaiting their validation by a human person. Such contributors can be researchers, people in charge of communication effort. Or members of the Wikipedia community.


See also:
* Karima Rafes https://io.datascience-paris-saclay.fr/appDisplayScientists.php
* Denis Humbert https://io.datascience-paris-saclay.fr/datasetView.php?dataset_id=29
* Wikidata Mix'nMatch Tool https://tools.wmflabs.org/mix-n-match/
